// -*- compile-command: "make all"; -*-
#pragma once
#include "cubeData.h"
#include "util.h"

namespace cube4 {
  const int N_M = 184;
  char const* faces = "TRUFKLDB";
  string moves[N_M];

  void init() {
    cubeData cd4(4);
    assert(N_M == 184);
  }
}
