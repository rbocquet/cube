// -*- compile-command: "make all"; -*-

#pragma once
#include "cubeData.h"
#include "cube2_4.h"

#undef all
#include <SDL.h>
#include <Magnum/Renderer.h>
#include <Magnum/Platform/Platform.h>
#include <Magnum/Primitives/Cube.h>
#include <Magnum/Primitives/Square.h>
#include <Magnum/DefaultFramebuffer.h>
#include <Magnum/Platform/Sdl2Application.h>
#include <Magnum/Primitives/Square.h>
#include <Magnum/Trade/MeshData3D.h>
#include <Magnum/Math/Matrix.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Magnum.h>
#include <Magnum/Buffer.h>
#include <Magnum/MeshTools/CompressIndices.h>
#include <Magnum/MeshTools/Interleave.h>
#include <Magnum/MeshTools/Duplicate.h>
#include <Magnum/Mesh.h>
#include <Magnum/Shaders/Phong.h>
#include <Magnum/Shaders/Flat.h>
#include <Magnum/Shaders/MeshVisualizer.h>
#include <Magnum/Color.h>
#define all(x) begin(x),end(x)

using namespace Magnum;
const Magnum::Color3 colors[4][2] = {
  {Color3(255,255,255)/255.0,Color3(255,213,0)/255.0}, // W,Y
  {Color3(196,30,58)/255.0,Color3(255,88,0)/255.0},    // R,O
  {Color3(0,81,177)/255.0,Color3(0,158,96)/255.0},     // B,G
  {Color3(0,255,255)/255.0,Color3(255,0,255)/255.0}    // ?,?
};

struct CubeViewer2_4 : Platform::Application {
  cubeData cd4;
  vector<cube2_4> cubes;
  int cubei = 0;
  Matrix4 transformation, projection;

  explicit CubeViewer2_4(int argc, char** argv, vector<cube2_4> c) :
    Platform::Application(Arguments(argc, argv)),
    cd4(4), cubes(c) {
    Renderer::enable(Renderer::Feature::DepthTest);

    transformation = Matrix4::rotationX(30.0_degf)*
      Matrix4::rotationY(40.0_degf);
    projection = Matrix4::perspectiveProjection(35.0_degf, Vector2{defaultFramebuffer.viewport().size()}.aspectRatio(), 0.01f, 100.0f)*
      Matrix4::translation(Vector3::zAxis(-10.0f));
  }

  void drawCube(int col, int f, vi const& de) {
    if(f==4) return; // Kata

    Vector3 base;
    if(f%4==0) base = Vector3(0,0,0);
    if(f%4==1) base = Vector3(1,0,0);
    if(f%4==2) base = Vector3(0,1,0);
    if(f%4==3) base = Vector3(0,0,1);
    if(f>=4) base=-base;
    Vector3 B[4] = {- base, Vector3(1,0,0), Vector3(0,1,0), Vector3(0,0,1)};
    base *= 2;
    for(int i : de) if(i!=f) {
        if(i<4) base += B[i]/3;
        else base -= B[i-4]/3;
    }

    Trade::MeshData3D cubeM = Primitives::Cube::solid();
    for(Vector3& v : cubeM.positions(0)) {
      v /= 8;
      v += base;
    }
    Buffer vertexBuffer, indexBuffer;
    vertexBuffer.setData(cubeM.positions(0), BufferUsage::StaticDraw);

    Containers::Array<char> indexData;
    Mesh::IndexType indexType;
    UnsignedInt indexStart, indexEnd;
    std::tie(indexData, indexType, indexStart, indexEnd) = MeshTools::compressIndices(cubeM.indices());
    indexBuffer.setData(indexData, BufferUsage::StaticDraw);

    int vertexCount = cubeM.positions(0).size();
    vector<Float> vertexIndex(vertexCount);
    std::iota(all(vertexIndex), 0.0f);
    Buffer vertexIndices;
    vertexIndices.setData(vertexIndex, BufferUsage::StaticDraw);


    Mesh mesh;
    mesh.setPrimitive(cubeM.primitive())
      .setCount(cubeM.indices().size())
      .addVertexBuffer(vertexBuffer, 0, Shaders::MeshVisualizer::Position{})
      .setIndexBuffer(indexBuffer, 0, indexType, indexStart, indexEnd)
      .addVertexBuffer(vertexIndices, 0, Shaders::MeshVisualizer::VertexIndex{});

    Shaders::MeshVisualizer ff;
    ff.setColor(colors[col%4][col>=4])
      // .setWireframeColor(colors[col%4][1-(col>=4)])
      // .setWireframeWidth(10.0)
      .setViewportSize((Vector2)defaultFramebuffer.viewport().size())
      .setTransformationProjectionMatrix(projection*transformation);
    mesh.draw(ff);
  }

  virtual void drawEvent() override {
    defaultFramebuffer.clear(FramebufferClear::Color | FramebufferClear::Depth);
    if(cubes.size()) {
      auto cube = cubes[cubei];
      FOR(i, cube2_4_ns::N_c4) {
        tpl<vi, vi> p = cd4.positions[4][cube.c4[i]];
        FOR(j, 4) drawCube(cd4.cubies[4][i][j], p.x()[p.y()[j]], p.x());
      }
    }
    swapBuffers();
  }

  virtual void keyPressEvent(KeyEvent& k) override {
    switch(k.key()){
    case KeyEvent::Key::Left:
      if(cubei>0) cubei-=1;
      redraw();
      break;
    case KeyEvent::Key::Right:
      if(cubei+1<(int)cubes.size()) cubei+=1;
      redraw();
      break;
    default: break;
    }
  }
};
