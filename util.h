// -*- compile-command: "make all"; -*-
#pragma once
#include "header.h"

uint8_t* loadFileOr(char const* filename, size_t size, function<void(uint8_t*)> doGen) {
  uint8_t* data = (uint8_t*)malloc(size); assert(data);
  FILE* f = fopen(filename, "rb");
  if(!f) goto gen;
  if(fread((char*)data, 1, size, f) != size) {
    fclose(f);
    assert(false);
  }
  fclose(f);
  return data;
 gen:
  doGen(data);
  f = fopen(filename, "wb");
  if(!f) assert(false);
  if(fwrite((char*)data, 1, size, f) != size){
    fclose(f);
    assert(false);
  }
  fclose(f);
  return data;
}
