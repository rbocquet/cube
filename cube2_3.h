// -*- compile-command: "make all"; -*-
#pragma once
#include "cubeData.h"
#include "sparsepp.h"

namespace cube2_3_ns {
  const int N_c3  = 8;
  const int NP_c3 = 24;
  const int N_M   = 18;
  const int N_S   = 24;

  uint8_t move_table_3[N_M][NP_c3];
  uint8_t inv_move_table[N_M];
  bool transition_table[N_M+1][N_M+1];

  uint16_t sym_inv_table[N_S];
  uint16_t sym_mul_table[N_S][N_S];
  uint8_t sym_cubie_table_3[N_S][N_c3];
  uint8_t sym_pos_table_3[N_S][NP_c3];

  struct cube2_3 {
    uint8_t c3[N_c3] = {0};

    cube2_3(){}

    void move(uint8_t m) {
      uint8_t* p = move_table_3[m];
      c3[0] = p[c3[0]]; c3[1] = p[c3[1]]; c3[2] = p[c3[2]]; c3[3] = p[c3[3]];
      c3[4] = p[c3[4]]; c3[5] = p[c3[5]]; c3[6] = p[c3[6]]; c3[7] = p[c3[7]];
    }

    void moveTo(cube2_3& c, uint8_t m){
      uint8_t* p = move_table_3[m];
      c.c3[0] = p[c3[0]]; c.c3[1] = p[c3[1]]; c.c3[2] = p[c3[2]]; c.c3[3] = p[c3[3]];
      c.c3[4] = p[c3[4]]; c.c3[5] = p[c3[5]]; c.c3[6] = p[c3[6]]; c.c3[7] = p[c3[7]];
    }

    void sym(cube2_3& c, uint16_t s) {
      int s_ = sym_inv_table[s];
      FOR(i, N_c3) c.c3[i] = sym_pos_table_3[s][c3[sym_cubie_table_3[s_][i]]];
    }

    void sym2(cube2_3& c, uint16_t s){
      FOR(i, N_c3) c.c3[i] = sym_pos_table_3[s][c3[i]];
    }

    XXH64_hash_t hash() const {
      return XXH64((char const*)this, sizeof(cube2_3), 42);
    }

    void print() {
      FOR(i, N_c3) cout << (int)c3[i] << " ";
      cout << endl;
    }

    void display() {
      cubeData cd3(3);

      int C[6][2][2]={0};
      char const* cols[] = { C_RESET "X",
                             C_BBLUE "X",
                             C_BRED "X",
                             C_BYELLOW "X",
                             C_BGREEN "X",
                             C_BCYAN "X"
      };

      FOR(i, N_c3) {
        tpl<vi, vi> p = cd3.positions[3][c3[i]];
        FOR(j, 3) {
          int c = cd3.cubies[3][i][j];
          int f = p.x()[p.y()[j]];
          int d1=-1, d2=-1;
          if(f==0) { d1=2; d2=4; }
          if(f==1) { d1=3; d2=2; }
          if(f==2) { d1=3; d2=4; }
          if(f==3) { d1=5; d2=4; }
          if(f==4) { d1=3; d2=5; }
          if(f==5) { d1=3; d2=1; }
          int a=0, b=0;
          FOR(k, 3) if(k!=j) {
            if(p.x()[p.y()[k]]==d1) a+=1;
            if(p.x()[p.y()[k]]==d2) b+=1;
          }
          C[f][a][b]=c;
        }
      }

      FOR(i,2) {
        cout << "   ";
        FOR(j,2) cout << cols[C[0][i][j]];
        cout << endl;
      }
      cout << endl;
      FOR(i, 2) {
        FOR(j, 2) cout << cols[C[1][i][j]]; cout << " ";
        FOR(j, 2) cout << cols[C[2][i][j]]; cout << " ";
        FOR(j, 2) cout << cols[C[4][i][j]]; cout << " ";
        FOR(j, 2) cout << cols[C[5][i][j]]; cout << endl;
      }
      cout << endl;
      FOR(i,2) {
        cout << "   ";
        FOR(j,2) cout << cols[C[3][i][j]];
        cout << endl;
      }
      cout << endl;
      cout << C_RESET << flush;
    }

  };

  static cube2_3 initial;

  void init() {
    static bool done = 0;
    if(done) return;
    done = 1;
    cubeData cd3(3);
    assert(N_c3 == cd3.cubies[3].size());
    assert(NP_c3 == cd3.positions[3].size());
    assert(N_M == cd3.moves.size());
    calcMoveTable<N_M, N_c3, NP_c3>(move_table_3, cd3, 3);
    calcInvMoveTable<N_M>(cd3, inv_move_table);
    calcTransitionTable<N_M>(cd3, transition_table);
    calcSymTables<N_S>(cd3, sym_inv_table, sym_mul_table);
    calcSymCubiePosTables<N_S, N_c3, NP_c3>(cd3, 3, sym_cubie_table_3, sym_pos_table_3);
    FOR(i, N_c3) initial.c3[i] = cd3.initial[3][i];
  }

  vi solve(cube2_3 const& c) {
    int md=0;
    cube2_3 C0[50], C1[50]; C0[0] = initial; C1[0] = c;
    while(1) {
      md+=1;
      spp::sparse_hash_map<XXH64_hash_t, uint8_t> S0, S1;
      int niter0=0, niter1=0;
      function<void(int, int)> dfs0 = [&](int d, int m) {
        niter0+=1;
        if(!(niter0&262143)) { cout << 0 << " " << md << " " << niter0 << flush << P_BEGIN; }
        auto h = C0[d].hash();
        if(S0.count(h)&&S0[h]<=d) return;
        S0[h]=d;
        if(d == md) return;
        FOR(i, N_M) if(transition_table[m][i]){
          C0[d].moveTo(C0[d+1], i); dfs0(d+1, i);
        }
      };
      cube2_3 middle;
      int best=-1;
      function<void(int, int)> dfs1 = [&](int d, int m) {
        niter1+=1;
        if(!(niter1&262143)) { cout << 1 << " " << md << " " << niter1 << flush << P_BEGIN; }
        auto h = C1[d].hash();
        if(S0.count(h)&&(best==-1||d<best)) {
          best=d;
          S1[h]=d;
          middle = C1[d];
          return;
        }
        if(d == md) return;
        if(S1.count(h)&&S1[h]<=d) return;
        S1[h]=d;
        FOR(i, N_M) if(transition_table[m][i]){
          C1[d].moveTo(C1[d+1], i);
          dfs1(d+1, i);
        }
      };
      auto rebuild = [&](vi& V, spp::sparse_hash_map<XXH64_hash_t, uint8_t> const& S) {
        V.clear();
        cube2_3 c = middle;
        while(1) {
          auto h = c.hash();
          if(S.at(h)==0) return;
          FOR(i, N_M) {
            cube2_3 c_; c.moveTo(c_, i);
            auto h_ = c_.hash();
            if(S.count(h_) && S.at(h_)<S.at(h)) {
              V.pb(i);
              c = c_;
              goto l_rebuild_end;
            }
          }
          assert(false);
        l_rebuild_end:;
        }
      };
      FOR(i, N_S) {
        initial.sym2(C0[0], i);
        dfs0(0, N_M);
      }
      dfs1(0, N_M);
      if(best!=-1){
        cout << "depth:" << md << " " << niter0 << " " << niter1 << endl;
        vi MS0, MS1;
        rebuild(MS1, S1);
        rebuild(MS0, S0);
        reverse(all(MS1)); for(auto& i : MS1) i = inv_move_table[i];
        MS1.insert(end(MS1), all(MS0));
        return MS1;
      }
      cout << "depth:" << md << " " << niter0 << " " << niter1 << endl;
    }
  }

}

using cube2_3_ns::cube2_3;

/*
 *
 */
