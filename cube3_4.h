// -*- compile-command: "make all"; -*-
#pragma once
#include "cubeData.h"

namespace cube3_4_ns {
  const int N_c2  = 24;
  const int NP_c2 = 48;
  const int N_c3  = 32;
  const int NP_c3 = 192;
  const int N_c4  = 16;
  const int NP_c4 = 192;
  const int N_M   = 184;

  const int MAX_BINOM = 48+2;
  uint64_t binom[MAX_BINOM][MAX_BINOM];

  uint8_t pos_base_2[NP_c2];
  uint8_t pos_base_3[NP_c3];
  uint8_t pos_base_4[NP_c4];

  bool cub_c2_1[N_c2];
  bool cub_c3_1[N_c3];
  bool pos_c2_1[NP_c2];
  bool pos_c3_1[NP_c3];
  const int N_table_1_2 = 2704156;  // binomial(24,12)
  uint8_t table_1_2[N_table_1_2];
  const int N_table_1_3 = 10518300; // binomial(32,8)
  uint8_t table_1_3[N_table_1_3];

  uint8_t move_table_2[N_M][NP_c2];
  uint8_t move_table_3[N_M][NP_c3];
  uint8_t move_table_4[N_M][NP_c4];

  struct cube3_4 {
    uint8_t c2[N_c2] = {0};
    uint8_t c3[N_c3] = {0};
    uint8_t c4[N_c4] = {0};

    cube3_4(){}

    void moveTo(cube3_4& c, uint8_t m){
      uint8_t* p = move_table_2[m];
      FOR(i, N_c2) c.c2[i]=p[c2[i]];
      p = move_table_3[m];
      FOR(i, N_c3) c.c3[i]=p[c3[i]];
      p = move_table_4[m];
      FOR(i, N_c4) c.c4[i]=p[c4[i]];
    }

    void move(uint8_t m) {
      uint8_t* p = move_table_2[m];
      FOR(i, N_c2) c2[i]=p[c2[i]];
      p = move_table_3[m];
      FOR(i, N_c3) c3[i]=p[c3[i]];
      p = move_table_4[m];
      FOR(i, N_c4) c4[i]=p[c4[i]];
    }

    void print() {
      FOR(i, N_c2) cout << (int)c2[i] << " ";
      FOR(i, N_c3) cout << (int)c3[i] << " ";
      FOR(i, N_c4) cout << (int)c4[i] << " ";
      cout << endl;
    }

    uint32_t phase1_val2() const {
      uint32_t x = 0;
      FOR(i, N_c2) if(cub_c2_1[i]) x |= (1<<pos_base_2[c2[i]]);
      int n=24, k=12;
      uint32_t y = 0;
      FOR(i, 24) {
        if(x&(1<<i)) {
          k -= 1;
          if(k==0) break;
        }else{
          y += binom[n-1][k-1];
        }
        n-=1;
      }
      return y;
    }

    uint32_t phase1_val3() const {
      uint32_t x = 0;
      FOR(i, N_c3) if(cub_c3_1[i]) x |= (1<<pos_base_3[c3[i]]);
      int n=32, k=8;
      uint32_t y = 0;
      FOR(i, 32) {
        if(x&(1<<i)) {
          k -= 1;
          if(k==0) break;
        }else{
          y += binom[n-1][k-1];
        }
        n-=1;
      }
      return y;
    }

    XXH64_hash_t hash() const {
      return XXH64((char const*)this, sizeof(cube3_4), 42);
    }
  };

  static cube3_4 initial;

  void init_binom() {
    memset(binom, 0, sizeof(binom));
    FOR(i, MAX_BINOM) {
      binom[i][0]=binom[i][i]=1;
      FORU(j, 1, i-1) {
        binom[i][j]=binom[i-1][j-1]+binom[i-1][j];
      }
    }
  }

  // void init_phase1() {
  //   cubeData cd(4);
  //   FOR(i, N_c2) {
  //     cub_c2_1[i]=1;
  //     FOR(j, 2) if(cd.cubies[2][i][j]%4==0) cub_c2_1[i] = 0;
  //   }
  //   FOR(i, N_c3) {
  //     cub_c3_1[i]=1;
  //     FOR(j, 3) if(cd.cubies[3][i][j]%4==0) cub_c3_1[i] = 0;
  //   }
  //   FOR(i, NP_c2) {
  //     pos_c2_1[i] = 1;
  //     FOR(j, 2) if(cd.positions[2][i].x()[j]%4 == 0) pos_c2_1[i] = 0;
  //   }
  //   FOR(i, NP_c3) {
  //     pos_c3_1[i] = 1;
  //     FOR(j, 3) if(cd.positions[3][i].x()[j]%4 == 0) pos_c3_1[i] = 0;
  //   }

  //   { memset(table_1_2, 0, sizeof(table_1_2));
  //     queue<tpl<int,cube3_4>> Q;
  //     auto add = [&](cube3_4 const& c, int i, int d) { table_1_2[i]=d; Q.push(mt(d,c)); };
  //     add(initial, initial.phase1_val2(), 0); table_1_2[initial.phase1_val2()]=1;
  //     int nadd=0;
  //     while(!Q.empty()) {
  //       nadd+=1;
  //       if(nadd%10000==0) { cout << nadd << "/" << N_table_1_2 << " " << Q.size() << endl; }
  //       int d = Q.front().x();
  //       cube3_4 c_ = Q.front().y();
  //       Q.pop();
  //       FOR(m, N_M) {
  //         cube3_4 c; c_.moveTo(c, m);
  //         int j = c.phase1_val2();
  //         if(table_1_2[j]==0) add(c, j, d+1);
  //       }
  //     }
  //     table_1_2[initial.phase1_val2()]=0;
  //     map<int, int> M;
  //     FOR(i, N_table_1_2) M[table_1_2[i]]+=1;
  //     for(auto p : M) cout << p << endl;
  //   }

  //   { memset(table_1_3, 0, sizeof(table_1_3));
  //     queue<tpl<int,cube3_4>> Q;
  //     auto add = [&](cube3_4 const& c, int i, int d) { table_1_3[i]=d; Q.push(mt(d,c)); };
  //     add(initial, initial.phase1_val3(), 0); table_1_3[initial.phase1_val3()]=1;
  //     int nadd=0;
  //     while(!Q.empty()) {
  //       nadd+=1;
  //       if(nadd%10000==0) { cout << nadd << "/" << N_table_1_3 << " " << Q.size() << endl; }
  //       int d = Q.front().x();
  //       cube3_4 c_ = Q.front().y();
  //       Q.pop();
  //       FOR(m, N_M) {
  //         cube3_4 c; c_.moveTo(c, m);
  //         int j = c.phase1_val3();
  //         if(table_1_3[j]==0) add(c, j, d+1);
  //       }
  //     }
  //     table_1_3[initial.phase1_val3()]=0;
  //     map<int, int> M;
  //     FOR(i, N_table_1_3) M[table_1_3[i]]+=1;
  //     for(auto p : M) cout << p << endl;
  //   }
  // }

  void init() {
    static bool done = 0;
    if(done) return;
    done = 1;
    init_binom();

    cubeData cd(4);
    assert(N_c2 == cd.cubies[2].size());
    assert(NP_c2 == cd.positions[2].size());
    assert(N_c3 == cd.cubies[3].size());
    assert(NP_c3 == cd.positions[3].size());
    assert(N_c4 == cd.cubies[4].size());
    assert(NP_c4 == cd.positions[4].size());
    assert(N_M == cd.moves.size());

    calcMoveTable<N_M, N_c2, NP_c2>(move_table_2, cd, 2);
    calcMoveTable<N_M, N_c3, NP_c3>(move_table_3, cd, 3);
    calcMoveTable<N_M, N_c4, NP_c4>(move_table_4, cd, 4);

    FOR(i, N_c2) initial.c2[i] = cd.initial[2][i];
    FOR(i, N_c3) initial.c3[i] = cd.initial[3][i];
    FOR(i, N_c4) initial.c4[i] = cd.initial[4][i];

    FOR(i, NP_c2) pos_base_2[i] = i/2;
    FOR(i, NP_c3) pos_base_3[i] = i/6;
    FOR(i, NP_c4) pos_base_4[i] = i/12;

    // init_phase1();
  }
}
using cube3_4_ns::cube3_4;
