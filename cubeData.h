// -*- compile-command: "make all"; -*-
#pragma once
#include "header.h"
#include "xxhash.h"

// convention : a piece (edge/corner) is oriented with its lowest face index.
// a position for a piece is oriented with the orientation of the default piece
// to compute the difference from a move : difference of orientations relative to the rotated face

bool permutation_parity(vi const& p) {
  int n = p.size();
  vi E(n);
  function<int(int)> dfs = [&](int i) {
    if(E[i]) return 0;
    E[i] = 1;
    return dfs(p[i])^1;
  };
  bool pa=0;
  FOR(i, n) if(!E[i]) pa^=1^dfs(i);
  return pa;
}

vi permutation_inverse(vi const& a) {
  vi c(a.size()); FOR(i, a.size()) c[a[i]] = i;
  return c;
}

vi permutation_compose(vi const& a, vi const& b) {
  assert(a.size()==b.size());
  vi c(a.size());
  FOR(i, a.size()) c[i]=b[a[i]];
  return c;
}

struct cubeData {
  int dim;
  int nface;

  using posi = tpl<vi, vi>;
  using move = tpl<int, vi>;

  vector<vvi> cubies;
  vector<unordered_map<vi, int> > rcubies;

  vvi initial;

  vv<posi> positions;
  vector<unordered_map<posi, int> > rpositions;

  vector<move> moves;
  unordered_map<move, int> rmoves;

  vector<vi> syms;
  unordered_map<vi, int> rsyms;

  cubeData(int d_) : dim(d_), nface(2*dim),
                     cubies(dim+1), rcubies(dim+1), initial(dim+1), positions(dim+1), rpositions(dim+1) {
    iterCubies([&](vi const& c){ rcubies[c.size()][c]=cubies[c.size()].size(); cubies[c.size()].pb(c); });
    FORU(i, 2, dim) for(vi const& c : cubies[i]) {
      initial[i].pb(positions[i].size());
      vi perm(c.size()); iota(all(perm), 0);
      do {
        if((int)c.size()==dim && permutation_parity(perm)) continue;
        rpositions[i][mt(c, perm)] = positions[i].size();
        positions[i].pb(mt(c, perm));
      } while(next_permutation(all(perm)));
    }
    iterMoves([&](int i, vi const& p){ rmoves[mt(i, p)] = moves.size(); moves.pb(mt(i, p)); });
    iterSyms([&](vi s) { rsyms[s]=syms.size(); syms.pb(s); });
  }

  vi norm_cubie(vi const& c) {
    vi d = c; sort(all(d), [&](int x, int y) { return axis(x)<axis(y); });
    bool p = 0; FOR(i, c.size()) p ^= d[i]>=dim?1:0;
    if(p) swap(d[c.size()-2], d[c.size()-1]);
    return d;
  }

  void iterCubies(function<void(vi const&)> f) {
    vi cur;
    function<void(int)> go = [&](int i){
      if(i == dim) {
        if(cur.size()>=2) f(norm_cubie(cur));
        return;
      }
      go(i+1);
      cur.pb(i); go(i+1); cur.pop_back();
      cur.pb(i+dim); go(i+1); cur.pop_back();
    };
    go(0);
  }

  int axis(int f) { return f%dim; }

  void iterMoves(function<void(int, vi)> f) {
    FOR(i, nface) {
      vi perm(dim); iota(all(perm), 0);
      bool identity=true;
      do {
        if(perm[axis(i)] != axis(i)) continue;
        FOR(m, 1<<dim) if(!(m&(1<<axis(i)))) {
          bool p = __builtin_parity(m) ^ permutation_parity(perm);
          if(p==1) continue;
          vi perm2 = perm;
          FOR(j, dim) if(m&(1<<j)) perm2[j] += dim;
          if(identity) { identity=false; continue; }
          f(i, perm2);
        }
      } while(next_permutation(all(perm)));
    }
  }

  void iterSyms(function<void(vi)> f) {
    vi perm(dim); iota(all(perm), 0);
    do {
      FOR(m, 1<<dim) {
        bool p = __builtin_parity(m) ^ permutation_parity(perm);
        if(p==1) continue;
        vi perm2=perm;
        FOR(j, dim) if(m&(1<<j)) perm2[j]+=dim;
        f(perm2);
      }
    } while(next_permutation(all(perm)));
  }

#define AP0(a,x) ((a[x%dim]+x-x%dim)%nface)

  posi applyMove(posi const& p, move const& mv) {
    auto const& b = p.x(); auto const& perm = p.y();
    int mf = mv.x(); auto const& mp = mv.y();
    if(find(all(b), mf) == end(b)) return mt(b, perm);
    vi nb = b; for(int& x : nb) x = AP0(mp,x);
    vi nb_ = norm_cubie(nb);
    int m = perm.size();
    vi perm2(m);
    FOR(i, m) FOR(j, m) if(nb[i]==nb_[j]) perm2[i]=j;
    return mt(nb_, permutation_compose(perm, perm2));
  }

  move invMove(move const& mv) {
    move m = mv;
    FOR(i, dim) m.y()[mv.y()[i]%dim] = (i+mv.y()[i]-mv.y()[i]%dim)%nface;
    return m;
  }

  vi applySymCubie(vi const& c, vi const& sym) {
    vi d(c.size());
    FOR(i, c.size()) d[i] = AP0(sym, d[i]);
    return norm_cubie(c);
  }

  posi applySymPos(posi const& p, vi const& sym) {
    auto const& b = p.x(); auto const& perm = p.y();
    vi nb = b;
    for(int& x : nb) x = AP0(sym, x);
    vi nb_ = norm_cubie(nb);
    int m = perm.size();
    vi perm2(m);
    FOR(i, m) FOR(j, m) if(nb[i]==nb_[j]) perm2[i]=j;
    return mt(nb_, permutation_compose(perm, perm2));
  }

  vi invSym(vi const& a) {
    vi c(dim); FOR(i, dim) c[a[i]%dim] = (i+a[i]-a[i]%dim)%nface;
    return c;
  }

  vi multSym(vi const& a, vi const& b) {
    vi c(dim); FOR(i, dim) c[i] = AP0(a, AP0(b, i));
    return c;
  }

  void printInfo() const {
    cout << dim << "-dimensional Rubik's cube" << endl;
    FORU(i, 2, dim) {
      cout << "  " << i << "-cubies : " << cubies[i].size() << endl;
      cout << "  " << i << "-positions : " << positions[i].size() << endl;
    }
    cout << "  " << moves.size() << " moves" << endl;
    cout << "  " << syms.size() << " syms" << endl;
    cout << endl;
  }
};

template<int M, int I, int IP>
void calcDist(uint8_t (&initial)[I], uint8_t (&move_table)[M][IP], uint8_t (&dist)[I][IP]){
  FOR(i, I) {
    memset(dist[i], -1, sizeof(dist[i]));
    queue<int> Q; auto add = [&](int j, int d){ dist[i][j] = d; Q.push(j); };
    add(initial[i], 0);
    while(!Q.empty()) {
      int j = Q.front(); Q.pop();
      FOR(m, M) {
        assert(0<=j&&j<IP); assert(0<=m&&m<M);
        int k = move_table[m][j];
        if(dist[i][k] == (uint8_t)-1) add(k, dist[i][j]+1);
      }
    }
  }
}

template<int M>
void calcInvMoveTable(cubeData& cd, uint8_t (&inv_move_table)[M]){
  FOR(i, M) {
    inv_move_table[i] = cd.rmoves.at(cd.invMove(cd.moves[i]));
  }
}

template<int M, int I, int IP>
void calcMoveTable(uint8_t (&move_table)[M][IP], cubeData& cd, int d){
  FOR(i, M) FOR(j, IP) {
    auto m = cd.applyMove(cd.positions[d][j], cd.moves[i]);
    move_table[i][j] = cd.rpositions[d].at(m);
  }
}

template<int M>
void calcTransitionTable(cubeData& cd, bool (&table)[M+1][M+1]) {
  assert(M == cd.moves.size());
  memset(table, 1, sizeof(table));
  FOR(i, M+1) table[i][M]=0;
  FOR(i, M) FOR(j, M) {
    if(i/(M/cd.nface) == j/(M/cd.nface)) table[i][j]=0;
    else if((i/(M/cd.nface))%(cd.dim) == (j/(M/cd.nface))%(cd.dim)) table[i][j]=i<j;
  }
}

template<int NS>
void calcSymTables(cubeData& cd, uint16_t (&invTable)[NS], uint16_t (&mulTable)[NS][NS]) {
  assert(NS == cd.syms.size());
  FOR(i, NS) invTable[i] = cd.rsyms.at(cd.invSym(cd.syms[i]));
  FOR(i, NS) FOR(j, NS) mulTable[i][j] = cd.rsyms.at(cd.multSym(cd.syms[i], cd.syms[j]));
}

template<int NS, int I, int IP>
void calcSymCubiePosTables(cubeData& cd, int d, uint8_t (&symCubieTable)[NS][I], uint8_t (&symPosTable)[NS][IP]) {
  assert(NS == cd.syms.size());
  assert(I == cd.cubies[d].size());
  assert(IP == cd.positions[d].size());
  FOR(i, NS) FOR(j, I) symCubieTable[i][j] = cd.rcubies[d].at(cd.applySymCubie(cd.cubies[d][j], cd.syms[i]));
  FOR(i, NS) FOR(j, IP) symPosTable[i][j] = cd.rpositions[d].at(cd.applySymPos(cd.positions[d][j], cd.syms[i]));
}

uint32_t binom[64][64];

struct initBinomS {
  initBinomS(){
    memset(binom,0,sizeof(binom));
    FOR(i, 64) {
      binom[i][0]=binom[i][i]=1;
      FORU(j,1,i-1) binom[i][j]=binom[i-1][j-1]+binom[i-1][j];
    }
  }
} initBinom;

uint32_t reduceBinom(uint32_t x, int n, int k) {
  uint32_t y=0;
  FOR(i, n) {
    if(x&(1<<i)) {
      k -= 1;
      if(k==0) return y;
    }else{
      y += binom[n-i-1][k-1];
    }
  }
  assert(k==0);
  return y;
}
