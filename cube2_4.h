// -*- compile-command: "make all"; -*-
#pragma once
#include "cubeData.h"
#include "cube4.h"
#include "cube2_3.h"
#include "util.h"

#include "sparsepp.h"

// KT UDFBLR

namespace cube2_4_ns {
  const int N_c4  = 16;
  const int NP_c4 = 192;
  const int N_M   = 184;
  const int N_S   = 192;

  uint8_t move_table_4[N_M][NP_c4];

  uint8_t inv_move_table[N_M];
  bool transition_table[N_M+1][N_M+1];

  uint16_t sym_inv_table[N_S];
  uint16_t sym_mul_table[N_S][N_S];
  uint8_t sym_cubie_table_4[N_S][N_c4];
  uint8_t sym_pos_table_4[N_S][NP_c4];

  bool center_4[N_c4];
  uint8_t pos_base_4[NP_c4];
  uint8_t pos_face_4[NP_c4];
  uint8_t pos_signature_4[NP_c4];

  uint8_t dist_4[N_c4][NP_c4];

  const int n_phase1_1 = 843448320;
  // uint8_t *d_phase1_1;
  // char const* f_phase1_1 = "data/cube2_4_phase1_1";
  const int n_phase1_2 = 843448320;
  // uint8_t *d_phase1_2;
  // char const* f_phase1_2 = "data/cube2_4_phase1_2";

  struct cube2_4 {
    uint8_t c4[N_c4];

    void moveTo(cube2_4& c, uint8_t m){
      uint8_t* p = move_table_4[m];
      FOR(i, N_c4) c.c4[i]=p[c4[i]];
    }

    void move(uint8_t m) {
      uint8_t* p = move_table_4[m];
      FOR(i, N_c4) c4[i]=p[c4[i]];
    }

    void sym(cube2_4& c, uint16_t s) {
      int s_ = sym_inv_table[s];
      FOR(i, N_c4) c.c4[i] = sym_pos_table_4[s][c4[sym_cubie_table_4[s_][i]]];
    }

    void sym2(cube2_4& c, uint16_t s){
      FOR(i, N_c4) c.c4[i] = sym_pos_table_4[s][c4[i]];
    }

    XXH64_hash_t hash() const {
      return XXH64((char const*)this, sizeof(cube2_4), 42);
    }

    void print() {
      FOR(i, N_c4) cout << (int)c4[i] << " ";
      cout << endl;
    }

    bool operator==(cube2_4 const& c) const { return memcmp(this, &c, sizeof(cube2_4)) == 0; }
    bool operator!=(cube2_4 const& c) const { return memcmp(this, &c, sizeof(cube2_4)) != 0; }
    bool operator<(cube2_4 const& c) const { return memcmp(this, &c, sizeof(cube2_4)) < 0; }
    bool operator<=(cube2_4 const& c) const { return memcmp(this, &c, sizeof(cube2_4)) <= 0; }
    bool operator>(cube2_4 const& c) const { return memcmp(this, &c, sizeof(cube2_4)) > 0; }
    bool operator>=(cube2_4 const& c) const { return memcmp(this, &c, sizeof(cube2_4)) >= 0; }

    // code the position of the T-colored stickers
    uint32_t val_phase1_1() const {
      uint32_t x = 0;
      uint32_t z = 0;
      uint8_t A[16];
      FOR(i, N_c4/2) {
        x |= (1<<pos_base_4[c4[i]]);
        A[pos_base_4[c4[i]]] = pos_face_4[c4[i]];
      }
      int j=0;
      FOR(i, 16) if(x&(1<<i)) {
        z |= (A[i]<<(2*j));
        j += 1;
      }
      uint32_t y = reduceBinom(x, 16, 8);
      uint32_t r = (y<<18)|(z<<2)|signature();
      return r;
    }

    uint32_t val_phase1_2() const {
      uint32_t x = 0;
      uint32_t z = 0;
      uint8_t A[16];
      FOR(i, N_c4/2) {
        x |= (1<<pos_base_4[c4[N_c4/2+i]]);
        A[pos_base_4[c4[N_c4/2+i]]] = pos_face_4[c4[N_c4/2+i]];
      }
      int j=0;
      FOR(i, 16) if(x&(1<<i)) {
        z |= (A[i]<<(2*j));
        j += 1;
      }
      uint32_t y = reduceBinom(x, 16, 8);
      uint32_t r = (y<<18)|(z<<2)|signature();
      return r;
    }

    // uint8_t manhattan() const {
    //   uint8_t r=0;
    //   FOR(i,N_c4) r+=dist_4[i][c4[i]];
    //   return r;
    // }

    // uint8_t h_phase1() const {
    //   return max(d_phase1_1[val_phase1_1()], d_phase1_2[val_phase1_2()]);
    // }

    uint8_t signature() const {
      uint8_t s=0;
      FOR(i, N_c4/2) s+=pos_signature_4[c4[i]];
      return s%3;
    }

    cube2_3 inner_2_3() {
      int mapF[8] = {-1,0,1,2,-1,3,4,5};
      cubeData cd3(3);
      cubeData cd4(4);
      cube2_3 c;
      FOR(i, N_c4/2) {
        auto p = cd4.positions[4].at(c4[i]);
        vi qx(3), qy(3);
        FOR(k, 3) qx[k] = mapF[p.x()[1+k]];
        FOR(k, 3) qy[k] = mapF[p.y()[1+k]];
        c.c3[i]=cd3.rpositions[3].at(mt(qx, qy));
      }
      return c;
    }

    cube2_3 outer_2_3() {
      int mapF[8] = {-1,0,1,2,-1,3,4,5};
      cubeData cd3(3);
      cubeData cd4(4);
      cube2_3 c;
      FOR(i, N_c4/2) {
        auto p = cd4.positions[4].at(c4[N_c4/2+i]);
        vi qx(3), qy(3);
        FOR(k, 3) qx[k] = mapF[p.x()[1+k]];
        FOR(k, 3) qy[k] = mapF[p.y()[1+k]];
        swap(qx[1], qx[2]);
        vi qy_(3); FOR(i,3)qy_[qy[i]]=i;
        c.c3[i]=cd3.rpositions[3].at(mt(qx, qy_));
      }
      return c;
    }

  };

  static cube2_4 initial;

  void init_signature() {
    cubeData cd4(4);
    FOR(i, NP_c4) {
      auto p = cd4.positions[4][i];
      FORU(j, 1, 3) if(p.y()[j] == 0) {
        swap(p.y()[0], p.y()[j]);
        swap(p.y()[1+((j-1)+1)%3], p.y()[1+((j-1)+2)%3]);
      }
      if(p.y()[1]==1) pos_signature_4[i]=0;
      else if(p.y()[1]==2) pos_signature_4[i]=1;
      else pos_signature_4[i]=2;
    }
  }

  void init() {
    cubeData cd4(4);

    assert(N_c4 == cd4.cubies[4].size());
    assert(NP_c4 == cd4.positions[4].size());
    assert(N_M == cd4.moves.size());
    assert(N_S == cd4.syms.size());

    calcInvMoveTable<N_M>(cd4, inv_move_table);
    calcMoveTable<N_M, N_c4, NP_c4>(move_table_4, cd4, 4);
    calcTransitionTable<N_M>(cd4, transition_table);
    calcSymTables<N_S>(cd4, sym_inv_table, sym_mul_table);
    calcSymCubiePosTables<N_S, N_c4, NP_c4>(cd4, 4, sym_cubie_table_4, sym_pos_table_4);

    FOR(i, N_c4) initial.c4[i] = cd4.initial[4][i];
    calcDist<N_M, N_c4, NP_c4>(initial.c4, move_table_4, dist_4);

    FOR(i, N_c4) {
      center_4[i]=0;
      for(int j : cd4.cubies[4][i]) if(j==0) center_4[i]=1;
    }
    FOR(i, NP_c4) pos_base_4[i] = cd4.rcubies[4][cd4.positions[4][i].x()];
    FOR(i, NP_c4) pos_face_4[i] = cd4.positions[4][i].y()[0];
    init_signature();
    // FOR(i, NP_c4) {
    //   auto p = cd4.positions[4][i];
    //   int nf=0; bool l=0;
    //   FOR(i,4) if(p.y()[i]!=i) {
    //     nf+=1;
    //     if(nf==2) l = p.y()[i]<i;
    //   }
    //   if(nf==3) pos_signature_4[i]=0; else pos_signature_4[i]=0;
    // }
  }

  // void init_phase1() {
  //   cubeData cd4(4); vi VS; FOR(i, N_S) if(cd4.syms[i][0]==0) VS.pb(i);

  //   d_phase1_1 = loadFileOr(f_phase1_1, n_phase1_1, [&](uint8_t* d_phase1_1) {
  //       memset(d_phase1_1, 0, n_phase1_1);
  //       queue<tpl<cube2_4, uint8_t, uint8_t> > Q;
  //       Q.push(mt(initial, 0, N_M)); d_phase1_1[initial.val_phase1_1()]=1;
  //       int niter=0;
  //       int nr=0;
  //       while(!Q.empty()) {
  //         DESTRUCT3(Q.front(), c, d, m); Q.pop();
  //         niter+=1;
  //         if(!(niter&2047)) cout << niter << " " << nr << " " << (int)d << " " << Q.size() << endl;
  //         cube2_4 X[N_M]; uint32_t V[N_M];
  //         FOR(i, N_M) if(transition_table[m][i]) {
  //           c.moveTo(X[i], i); V[i] = X[i].val_phase1_1();
  //         }
  //         const int K=23;
  //         FOR(i_, N_M/K) {
  //           FORU(i, K*i_, K*(i_+1)) __builtin_prefetch(&d_phase1_1[V[i]]);
  //           FORU(i, K*i_, K*(i_+1)) if(transition_table[m][i]) {
  //             if(d_phase1_1[V[i]]==0) {
  //               nr+=1; d_phase1_1[V[i]]=1;
  //               Q.push(mt(X[i], d+1, i));
  //               uint32_t R[24];
  //               FOR(j, 24) {
  //                 cube2_4 c_; X[i].sym(c_, VS[j]); R[j] = c_.val_phase1_1();
  //               }
  //               FOR(j, 24) __builtin_prefetch(&d_phase1_1[R[j]]);
  //               FOR(j, 24) if(d_phase1_1[R[j]]==0) {
  //                 nr+=1;
  //                 d_phase1_1[R[j]]=d+1;
  //               }
  //             }
  //           }
  //         }
  //       }
  //       d_phase1_1[initial.val_phase1_1()]=0;
  //     });

  //   d_phase1_2 = loadFileOr(f_phase1_2, n_phase1_2, [&](uint8_t* d_phase1_2) {
  //       memset(d_phase1_2, 0, n_phase1_2);
  //       queue<tpl<cube2_4, uint8_t, uint8_t> > Q;
  //       Q.push(mt(initial, 0, N_M)); d_phase1_2[initial.val_phase1_2()]=1;
  //       int niter=0;
  //       int nr=0;
  //       while(!Q.empty()) {
  //         DESTRUCT3(Q.front(), c, d, m); Q.pop();
  //         niter+=1;
  //         if(!(niter&2047)) cout << niter << " " << nr << " " << (int)d << " " << Q.size() << endl;
  //         cube2_4 X[N_M]; uint32_t V[N_M];
  //         FOR(i, N_M) if(transition_table[m][i]) {
  //           c.moveTo(X[i], i); V[i] = X[i].val_phase1_2();
  //         }
  //         const int K=23;
  //         FOR(i_, N_M/K) {
  //           FORU(i, K*i_, K*(i_+1)) __builtin_prefetch(&d_phase1_2[V[i]]);
  //           FORU(i, K*i_, K*(i_+1)) if(transition_table[m][i]) {
  //             if(d_phase1_2[V[i]]==0) {
  //               Q.push(mt(X[i], d+1, i));
  //               uint32_t R[24];
  //               FOR(j, 24) {
  //                 cube2_4 c_; X[i].sym(c_, VS[j]); R[j] = c_.val_phase1_2();
  //               }
  //               FOR(j, 24) __builtin_prefetch(&d_phase1_2[R[j]]);
  //               FOR(j, 24) if(d_phase1_2[R[j]]==0) {
  //                 nr+=1;
  //                 d_phase1_2[R[j]]=d+1;
  //               }
  //             }
  //           }
  //         }
  //       }
  //       d_phase1_2[initial.val_phase1_2()]=0;
  //     });
  // }

  vi phase1(cube2_4 const& c) {
    cubeData cd4(4);
    vi VM; FOR(i, cube2_4_ns::N_M) if(cd4.moves[i].x()>=4) VM.pb(i);

    auto getH = [&](cube2_4 const& c) { return mt(c.val_phase1_1(), c.val_phase1_2()); };

    int md=0;
    cube2_4 C0[50], C1[50]; C0[0] = initial; C1[0] = c;
    while(1) {
      md+=1;
      spp::sparse_hash_map<tpl<uint32_t, uint32_t>, uint8_t> S0, S1;
      S0.reserve(177852630);
      int niter0=0, niter1=0;
      function<void(int, int)> dfs0 = [&](int d, int m) {
        niter0+=1;
        if(!(niter0&262143)) { cout << 0 << " " << md << " " << niter0 << flush << P_BEGIN; }
        auto h = getH(C0[d]);
        if(S0.count(h)&&S0[h]<=d) return;
        S0[h]=d;
        if(d == md+1) return;
        for(int i : VM) if(transition_table[m][i]){
          C0[d].moveTo(C0[d+1], i); dfs0(d+1, i);
        }
      };
      cube2_4 middle;
      int best=-1;
      function<bool(int, int)> dfs1 = [&](int d, int m) {
        niter1+=1;
        if(!(niter1&262143)) { cout << 1 << " " << md << " " << niter1 << flush << P_BEGIN; }
        auto h = getH(C1[d]);
        if(S0.count(h)&&(best==-1||d<best)) {
          S1[h]=d;
          best=d;
          middle = C1[d];
          return 0;
        }
        if(d == md) return 0;
        if(S1.count(h)&&S1[h]<=d) return 0;
        S1[h]=d;
        for(int i : VM) if(transition_table[m][i]){
          C1[d].moveTo(C1[d+1], i);
          if(dfs1(d+1, i)) return 1;
        }
        return 0;
      };
      auto rebuild = [&](vi& V, spp::sparse_hash_map<tpl<uint32_t, uint32_t>, uint8_t> const& S) {
        V.clear();
        cube2_4 c = middle;
        while(1) {
          auto h = getH(c);
          if(S.at(h)==0) return;
          for(int i : VM) {
            cube2_4 c_; c.moveTo(c_, i);
            auto h_ = getH(c_);
            if(S.count(h_) && S.at(h_)<S.at(h)) {
              V.pb(i);
              c = c_;
              goto l_rebuild_end;
            }
          }
          assert(false);
        l_rebuild_end:;
        }
      };
      dfs0(0, N_M);
      dfs1(0, N_M);
      if(best!=-1) {
        cout << "depth:" << md << " " << niter0 << " " << niter1 << endl;
        vi MS0, MS1;
        rebuild(MS1, S1);
        rebuild(MS0, S0);
        reverse(all(MS1)); for(auto& i : MS1) i = inv_move_table[i];
        MS1.insert(end(MS1), all(MS0));
        return MS1;
      }
      cout << "depth:" << md << " " << niter0 << " " << niter1 << endl;
    }
  }

  // vi phase2(cube2_4 const& c) {
  //   cubeData cd4(4);
  //   vi VM; FOR(i, cube2_4_ns::N_M) {
  //     if(cd4.moves[i].x()==4) continue;
  //     if(cd4.moves[i].y()[0]==0) VM.pb(i);
  //   }
  //   auto getH = [&](cube2_4 const& c) { return c.hash(); };
  //   spp::sparse_hash_map<XXH64_hash_t, uint8_t> S0;
  //   const int md0=6;
  //   cube2_4 C0[50]; C0[0] = initial;
  //   int niter=0;
  //   function<void(int, int)> dfs0 = [&](int d, int m) {
  //     niter+=1;
  //     if(!(niter&262143)) { cout << niter << flush << P_BEGIN; }
  //     auto h = getH(C0[d]);
  //     if(S0.count(h)&&S0[h]<=d) return;
  //     S0[h]=d;
  //     if(d == md0) return;
  //     for(int i : VM) if(transition_table[m][i]){
  //         C0[d].moveTo(C0[d+1], i); dfs0(d+1, i);
  //       }
  //   };
  //   dfs0(0,N_M);
  //   cout << niter << "." << endl;
  //   cube2_4 C1[50]; C1[0]=c;
  //   int md=0;
  //   int niter1=0;
  //   int mr = 9999;
  //   function<void(int, int)> dfs1 = [&](int d, int m) {
  //     niter1+=1;
  //     if(!(niter1&4194303)) { cout << niter1 << flush << P_BEGIN; }
  //     auto h = getH(C1[d]);
  //     auto mv = C1[d].manhattan();
  //     if(8*d+mv>=md) {
  //       if(S0.count(h)) {
  //         cout << "OK" << endl;
  //         assert(false);
  //         return;
  //       }
  //       mr = min<int>(mr, mv);
  //       return;
  //     }
  //     for(int i : VM) if(transition_table[m][i]){
  //         C1[d].moveTo(C1[d+1], i); dfs1(d+1, i);
  //       }
  //   };
  //   while(1) {
  //     md+=8;
  //     dfs1(0,N_M);
  //     cout << md/8 << " " << niter1 << " " << mr << endl;
  //   }
  //   assert(false);
  // }

  vi phase2(cube2_4 const& c) {
    cubeData cd4(4);
    vi VM; FOR(i, cube2_4_ns::N_M) {
      if(cd4.moves[i].x()<4) continue;
      if(cd4.moves[i].y()[0]==0) VM.pb(i);
    }
    auto getH = [&](cube2_4 const& c) { return c.hash(); };

    int md=0;
    cube2_4 C0[50], C1[50]; C0[0] = initial; C1[0] = c;
    while(1) {
      md+=1;
      spp::sparse_hash_map<XXH64_hash_t, uint8_t> S0, S1;
      // S0.reserve(10000000000ll);
      // S1.reserve(10000000000ll);
      int niter0=0, niter1=0;
      function<void(int, int)> dfs0 = [&](int d, int m) {
        niter0+=1;
        if(!(niter0&262143)) { cout << 0 << " " << md << " " << niter0 << flush << P_BEGIN; }
        auto h = getH(C0[d]);
        if(S0.count(h)&&S0[h]<=d) return;
        S0[h]=d;
        if(d == md) return;
        for(int i : VM) if(transition_table[m][i]){
          C0[d].moveTo(C0[d+1], i); dfs0(d+1, i);
        }
      };
      cube2_4 middle;
      function<bool(int, int)> dfs1 = [&](int d, int m) {
        niter1+=1;
        if(!(niter1&262143)) { cout << 1 << " " << md << " " << niter1 << flush << P_BEGIN; }
        auto h = getH(C1[d]);
        if(S0.count(h)) {
          S1[h]=d;
          middle = C1[d];
          return 1;
        }
        if(d == md) return 0;
        if(S1.count(h)&&S1[h]<=d) return 0;
        S1[h]=d;
        for(int i : VM) if(transition_table[m][i]){
          C1[d].moveTo(C1[d+1], i);
          if(dfs1(d+1, i)) return 1;
        }
        return 0;
      };
      auto rebuild = [&](vi& V, spp::sparse_hash_map<XXH64_hash_t, uint8_t> const& S) {
        V.clear();
        cube2_4 c = middle;
        while(1) {
          auto h = getH(c);
          if(S.at(h)==0) return;
          for(int i : VM) {
            cube2_4 c_; c.moveTo(c_, i);
            auto h_ = getH(c_);
            if(S.count(h_) && S.at(h_)<S.at(h)) {
              V.pb(i);
              c = c_;
              goto l_rebuild_end;
            }
          }
          assert(false);
        l_rebuild_end:;
        }
      };
      // FOR(i, N_S) if(cd4.syms[i][0]==0) {
      //   initial.sym2(C0[0], i);
      //   dfs0(0, N_M);
      // }
      dfs0(0, N_M);
      if(dfs1(0, N_M)) {
        cout << "depth:" << md << " " << niter0 << " " << niter1 << endl;
        vi MS0, MS1;
        rebuild(MS1, S1);
        rebuild(MS0, S0);
        reverse(all(MS1)); for(auto& i : MS1) i = inv_move_table[i];
        MS1.insert(end(MS1), all(MS0));
        return MS1;
      }
      cout << "depth:" << md << " " << niter0 << " " << niter1 << endl;
    }

  }

  // void phase1(cube2_4 const& c) {
  //   cubeData cd4(4)spar"sparsepp.h"
  //   vi VM; FOR(i, cube2_4_ns::N_M) if(1||cd4.moves[i].x()<4) VM.pb(i);
  //   cout << VM.size() << endl;
  //   cube2_4 C[50]; C[0]=c;
  //   int cut=0;
  //   int lh=c.h_phase1();
  //   uint64_t niter;
  //   function<void(int, int)> dfs = [&](int d, int m) {
  //     int h = C[d].h_phase1();
  //     niter+=1;
  //     if(!(niter&1048575)) { cout << lh << " " << niter << flush << P_BEGIN; }
  //     lh=min(h, lh);
  //     if(d+h>cut) return;
  //     for(int i : VM) {
  //         C[d].moveTo(C[d+1], i);
  //         dfs(d+1, i);
  //       }
  //   };
  //   while(1) {
  //     niter=0;
  //     dfs(0,N_M);
  //     cout << cut << " " << lh << " " << niter << endl;
  //     cut+=1;
  //   }
  // }

}
using cube2_4_ns::cube2_4;
