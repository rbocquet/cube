// -*- compile-command: "make all"; -*-

#include "header.h"
#include "cubeData.h"
#include "cube2_3.h"
#include "cube3_3.h"
#include "visu2_4.h"
#include "cube2_4.h"
#include "cube3_4.h"

int main(int argc, char** argv){
  (void)argc; (void)argv;
  srand(44);

  cubeData cd3(3); cd3.printInfo();
  cubeData cd4(4); cd4.printInfo();

  cube2_3_ns::init();
  cube3_3_ns::init();
  cube2_4_ns::init();
  cube3_4_ns::init();

  // cube2_4_ns::init_phase1();

  vi VM; FOR(i, cube2_4_ns::N_M) if(cd4.moves[i].x()>=4) VM.pb(i);
  int K; cin >> K;
  cube2_4 c=cube2_4_ns::initial;
  // vi A; FOR(i, K) A.pb(VM[rand()%VM.size()]);
  FOR(i, K) c.move(VM[rand()%VM.size()]);

  vector<cube2_4> cs; cs.pb(c);
  vi ms = cube2_4_ns::phase1(c);
  for(int i : ms) {
    c.move(i); cs.pb(c);
  }
  cout << ms << endl;
  cout << (int)c.signature() << endl;

  // cube2_4_ns::phase2(c);

  int mapF[8] = {-1,0,1,2,-1,3,4,5};
  int imapF[6] = {1,2,3,5,6,7};
  uint8_t mapM[cube2_3_ns::N_M];
  FOR(i, cube2_3_ns::N_M) {
    mapM[i]=-1;
    FOR(j, cube2_4_ns::N_M) {
      bool ok = 1;
      auto m = cd4.moves[j];
      if(mapF[m.x()] != cd3.moves[i].x()) ok=0;
      if(m.y()[0]!=0) ok=0;
      FOR(k, 3) if(mapF[m.y()[1+k]] != cd3.moves[i].y()[k]) ok = 0;
      if(ok) { mapM[i] = j; break; }
    }
    assert(mapM[i] != -1);
  }

  uint8_t mapM0[6][6];
  FOR(i, 6) FOR(j, 6) {
    mapM0[i][j]=-1;
    int i_ = imapF[i], j_ = imapF[j];
    FOR(k, cube2_4_ns::N_M) {
      if(cd4.moves[k].x()==0&&cd4.moves[k].y()[i_%4]==(j_+i_-i_%4)%8) {
        mapM0[i][j]=k;
        break;
      }
    }
    assert(mapM0[i][j]!=-1);
  }

  auto ms1 = cube2_3_ns::solve(c.inner_2_3());
  for(int i : ms1) {
    c.move(mapM[i]);
    cs.pb(c);
  }
  auto ms2 = cube2_3_ns::solve(c.outer_2_3());
  int first=-1;
  int last=-1;
  for(int i : ms2) {
    int next = cd3.moves[i].x();
    if(first==-1) {
      first=next;
    }else if(last!=next) {
      c.move(mapM0[last][next]);
      cs.pb(c);
    }
    last=next;
    c.move(mapM[i]);
    cs.pb(c);
  }
  if(last!=first) {
    c.move(mapM0[first][last]);
    cs.pb(c);
  }

  cout << ms.size() << " " << ms1.size() << " "  << ms2.size() << endl;
  cout << cs.size()-1 << endl;
  CubeViewer2_4 view(argc, argv, cs);
  view.exec();

  return 0;
}
