// -*- compile-command: "make all"; -*-
#include "cubeData.h"

namespace cube3_3_ns {
  const int N_c2  = 12;
  const int NP_c2 = 24;
  const int N_c3  = 8;
  const int NP_c3 = 24;
  const int N_M   = 18;

  uint8_t move_table_2[N_M][NP_c2];
  uint8_t move_table_3[N_M][NP_c3];

  uint8_t dist_2[N_c2][NP_c2];
  uint8_t dist_3[N_c3][NP_c3];

  struct cube3_3 {
    uint8_t c2[N_c2] = {0};
    uint8_t c3[N_c3] = {0};

    cube3_3(){}

    void move(uint8_t m) {
      uint8_t* p = move_table_2[m];
      c2[0] = p[c2[0]]; c2[1] = p[c2[1]]; c2[2] = p[c2[2]]; c2[3] = p[c2[3]];
      c2[4] = p[c2[4]]; c2[5] = p[c2[5]]; c2[6] = p[c2[6]]; c2[7] = p[c2[7]];
      c2[8] = p[c2[8]]; c2[9] = p[c2[9]]; c2[10] = p[c2[10]]; c2[11] = p[c2[11]];
      p = move_table_3[m];
      c3[0] = p[c3[0]]; c3[1] = p[c3[1]]; c3[2] = p[c3[2]]; c3[3] = p[c3[3]];
      c3[4] = p[c3[4]]; c3[5] = p[c3[5]]; c3[6] = p[c3[6]]; c3[7] = p[c3[7]];
    }

    void moveTo(cube3_3& c, uint8_t m){
      uint8_t* p = move_table_2[m];
      FOR(i, N_c2) c.c2[i]=p[c2[i]];
      p = move_table_3[m];
      FOR(i, N_c3) c.c3[i]=p[c3[i]];
    }

    uint8_t manhattan() const {
      uint8_t r1=0, r2=0;
      FOR(i,12) r1+=dist_2[i][c2[i]];
      FOR(i,8) r2+=dist_3[i][c3[i]];
      return max(r1,r2);
    }

    XXH64_hash_t hash() const {
      return XXH64((char const*)this, sizeof(cube3_3), 42);
    }

  };

  static cube3_3 initial;

  void init() {
    static bool done = 0;
    if(done) return;
    done = 1;
    cubeData cd(3);
    assert(N_c2 == cd.cubies[2].size());
    assert(NP_c2 == cd.positions[2].size());
    assert(N_c3 == cd.cubies[3].size());
    assert(NP_c3 == cd.positions[3].size());
    assert(N_M == cd.moves.size());
    calcMoveTable<N_M, N_c2, NP_c2>(move_table_2, cd, 2);
    calcMoveTable<N_M, N_c3, NP_c3>(move_table_3, cd, 3);

    FOR(i, N_c2) initial.c2[i] = cd.initial[2][i];
    FOR(i, N_c3) initial.c3[i] = cd.initial[3][i];

    calcDist<N_M, N_c2, NP_c2>(initial.c2, move_table_2, dist_2);
    calcDist<N_M, N_c3, NP_c3>(initial.c3, move_table_3, dist_3);
  }
}
using cube3_3_ns::cube3_3;
