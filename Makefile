all: main.cpp header.h
	g++ -std=c++1z -Wall -Wextra -I/usr/include/SDL2/ -o main main.cpp -march=native -lgmp -march=native -lX11 -lpthread -lMagnum -lMagnumPrimitives -lMagnumSdl2Application -lCorradeUtility -lGL -lSDL2 -lMagnumShaders -lMagnumMeshTools xxhash.c -Ofast -lstxxl -g -fopenmp -Wno-misleading-indentation
